const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Order = require('../models/order')
const Product = require('../models/product')
const Management  = require('../utility/management')

router.get('/', (req, res, next) => {
    this.modelObj.find()
        .select('-__v')
        .populate('product', '-__v')
        .exec()
        .then(docs => {
            const response = {
                count: docs.length,
                orders: docs.map(doc => { return Management.makeResponse(doc, req, false); })
            };
            res.status(200).json(response);
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({error: err});
        });
});

router.post('/', (req, res, next) => {
    Product.findById(req.body.product)
        .then(product => {
            if (!product)
                return res.status(404).json({
                    message: 'Product not found'
                });
            const order = new Order({
                _id: new mongoose.Types.ObjectId(),
                quantity: req.body.quantity,
                product: req.body.product
            });
            return order.save();
        })
        .then(result => {
            if (res.statusCode === 404)
                return res;
            console.log(result);
            delete result['_doc']['__v'];
            res.status(201).json({
                message: 'Created order successfully',
                createdOrder: Management.makeResponse(result, req, false)
            });
        })
        .catch(err => {
            res.status(500).json({
                message: 'Order creation failed',
                error: err
            });
        })
});

router.get('/:orderId', (req, res, next) => {
    const id = req.params.orderId;
    Order.findById(id)
        .select('-__v')
        .populate('product', '-__v')
        .exec()
        .then(doc => {
            console.log('From database', doc);
            if (doc) {
                res.status(200).json(doc);
            } else {
                res.status(404).json({message: 'No valid entry for provided ID' });
            }

        }).catch(err => {
        console.log(err);
        res.status(500).json({error: err});
    });
});

router.patch('/:orderId', (req, res, next) => {
    const id = req.params.orderId;
    const props = req.body;
    Order.updateOne({_id: id}, props)
        .exec()
        .then(result => {
            console.log(result);
            res.status(200).json({
                message: 'Order updated',
                ...Management.makeResponse(result, req, true)
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({error: err});
        });
});

router.delete('/:orderId', (req, res, next) => {
    const id = req.params.orderId;
    Order.deleteOne({_id: id}).exec()
        .then(result => {
            res.status(200).json({
                message: 'Order deleted'
            });
        })
        .catch(error => {
            console.log(error);
            res.status(500).json({error: error});
        });
});

module.exports = router;
