# Node REST Shop

Repository for my version* of the Node REST Shop tutorial program from the "[Creating a REST API with Node.js](https://www.youtube.com/watch?v=0oXYLzuucwE&list=PL55RiY5tL51q4D-B63KBnygU6opNPFk_q)" series by [Academind](https://www.youtube.com/channel/UCSJbGtTlrDami-tDGPUV9-w).

#
*Contains alterations based on personal knowledge and comment suggestions.